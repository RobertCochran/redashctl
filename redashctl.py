#!/usr/bin/env python3

# Copyright 2023 Robert Cochran <robert-git@cochranmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the “Software”), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import argparse
import json
import sys
import uuid
import os

from redash_toolbelt import Redash

parser = argparse.ArgumentParser(
    prog="redashctl",
    description="CLI interface for Redash",
)

parser.add_argument("redash_url")
subparsers = parser.add_subparsers(dest="command")

parser_get_data_sources = subparsers.add_parser("get-data-sources")

parser_get_data_source = subparsers.add_parser("get-data-source")
parser_get_data_source.add_argument("data_source_id", type=int)

parser_get_schema = subparsers.add_parser("get-schema")
parser_get_schema.add_argument("data_source_id", type=int)

parser_query = subparsers.add_parser("run-query", description="Reads query from stdin")
parser_query.add_argument("data_source_id", type=int)

parser_get_job = subparsers.add_parser("get-job")
parser_get_job.add_argument("job_id", type=uuid.UUID)

parser_get_query_result = subparsers.add_parser("get-query-results")
parser_get_query_result.add_argument("query_result_id", type=int)

parser_get_query = subparsers.add_parser("get-query")
parser_get_query.add_argument("query_id", type=int)

args = parser.parse_args()

def eprint(*args, **kwargs):
    return print(*args, **kwargs, file=sys.stderr)

redash_auth_file = os.getenv("REDASHCTL_AUTH_FILE")

if redash_auth_file is None:
    eprint("REDASHCTL_AUTH_FILE environment variable not set; you probably will need this")
    sys.exit(1)

redash_auth = json.load(open(redash_auth_file, "r"))

if args.redash_url not in redash_auth:
    eprint(f"No API key found for {args.redash_url}")
    sys.exit(1)

redash_api_key = redash_auth[args.redash_url]

redash = Redash(args.redash_url, redash_api_key)

# print(args)

def run_query(args):
    query = sys.stdin.read()
    json = {
        "query": query,
        "data_source_id": args.data_source_id,
        "max_age": 0,
        "parameters": {},
    }
    return redash._request("POST", f"api/query_results", json=json).json()

command_mapper = {
    "get-data-sources": lambda _: redash.get_data_sources(),
    "get-data-source": lambda a: redash.get_data_source(a.data_source_id),
    "get-schema": lambda a: redash._request("GET", f"api/data_sources/{a.data_source_id}/schema").json(),
    "run-query": run_query,
    "get-job": lambda a: redash._request("GET", f"api/jobs/{a.job_id}").json(),
    "get-query-results": lambda a: redash._request("GET", f"api/query_results/{a.query_result_id}").json(),
    "get-query": lambda a: redash.get_query(a.query_id),
}

print(json.dumps(command_mapper[args.command](args)))
