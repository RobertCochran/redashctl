;; -*- lexical-binding: t -*-

;; Copyright 2023 Robert Cochran <robert-git@cochranmail.com>
;;
;; This program is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later
;; version.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
;; details.
;;
;; You should have received a copy of the GNU General Public License along with
;; this program. If not, see <https://www.gnu.org/licenses/>.

(defcustom redashctl-enable-auto-job-scan t
  "Whether or not to automatically enable job scanning. Internally,
Redash queues each query up as a 'job', which you then use to
access the results. If you want to manually check a job to
display results, instead of using a timer, set this to `nil'. To
manually display results, use \[redashctl-show-results-for-job-queue]."
  :type boolean)

(defvar redashctl-urls '("http://localhost:5000"))
(defvar redashctl-refresh-timeout (* 60 60))

(defvar redashctl-current-url "http://localhost:5000")
(defvar redashctl-current-query-source "1")

(defvar redashctl--info-cache nil)
(defvar redashctl--job-queue nil)
(defvar redashctl--job-scan-timer nil
  "nil - no timer made
:disabled - explicitly disabled
<some other thing> - a real timer")

(require 'cl-lib)

(unless (json-available-p)
  (error "Native JSON unavailable"))

(defun redashctl--as-string (x)
  (format "%s" x))

(defun redashctl--as-iso-8601 (&optional x)
  (format-time-string "%FT%T%z" x))

(defun redashctl--iso-8601-to-unix (x)
  (time-convert (encode-time (iso8601-parse x)) 'integer))

;; (defun redashctl--nested-gethash (keys table &optional default)
;;   (let ((empty-hash-table (make-hash-table)))
;;     (cl-reduce (lambda (tab remaining-keys)
;; 		 (gethash (cl-first remaining-keys)
;; 			  tab
;; 			  (if (cl-rest remaining-keys)
;; 			      empty-hash-table
;; 			    default)))
;; 	       table keys)))

;; (defun redashctl--nested-puthash (value keys table)
;;   (cl-reduce (lambda (tab remaining-keys)
;; 	       (if (cl-rest remaining-keys)
;; 		   (gethash (cl-first remaining-keys) tab)
;; 		 (puthash (cl-first remaining-keys) tab)))
;; 	     table keys))

(defun redashctl--output-buffer-for-url (redash-url)
  (get-buffer-create (format "*redashctl-internal: %s*" redash-url) t))

(defun redashctl--results-buffer-for-url (redash-url)
  (get-buffer-create (format "*redashctl-results: %s*" redash-url)))

(defun redashctl--call-process (redash-url &rest args)
  (with-current-buffer (redashctl--output-buffer-for-url redash-url)
    (erase-buffer)
    (apply #'call-process
	   "redashctl" nil t nil
	   redash-url (mapcar #'redashctl--as-string args))))

(defun redashctl--call-process-region (redash-url start end &rest args)
  (let ((output-buffer (redashctl--output-buffer-for-url redash-url)))
    (with-current-buffer output-buffer
      (erase-buffer))
    (apply #'call-process-region
	   start end "redashctl"
	   nil output-buffer nil
	   redash-url (mapcar #'redashctl--as-string args))))

(defun redashctl--parse-results (redash-url)
  (with-current-buffer (redashctl--output-buffer-for-url redash-url)
    (goto-char (point-min))
    (json-parse-buffer)))

(defmacro redashctl--def-redash-fun (command-name fun-args regionp)
  (let ((fun-name (intern (concat "redashctl-" command-name)))
	(full-fun-args (append (if regionp '(start end)) fun-args)))
    `(defun ,fun-name ,(cons 'redash-url full-fun-args)
       ,(append (if regionp
		    '(redashctl--call-process-region)
		  '(redashctl--call-process))
		'(redash-url)
		(if regionp
		    '(start end))
		(list command-name)
		fun-args)
       (redashctl--parse-results redash-url))))

(redashctl--def-redash-fun "get-data-sources" () nil)
(redashctl--def-redash-fun "get-data-source" (data-source-id) nil)
(redashctl--def-redash-fun "get-schema" (data-source-id) nil)
(redashctl--def-redash-fun "run-query" (data-source-id) t)
(redashctl--def-redash-fun "get-job" (job-id) nil)
(redashctl--def-redash-fun "get-query-results" (query-result-id) nil)
(redashctl--def-redash-fun "get-query" (query-id) nil)

(defun redashctl--get-host-info (redash-url &optional refresh)
  (unless redashctl--info-cache
    (setf redashctl--info-cache (make-hash-table :test #'equal)))
  (let ((host-info (gethash redash-url redashctl--info-cache)))
    (when (or (not host-info)
	      refresh
	      (>= (- (time-convert nil 'integer)
		     (redashctl--iso-8601-to-unix (gethash "last_fetched" host-info)))
		  redashctl-refresh-timeout))
      (let ((sources (redashctl-get-data-sources redash-url))
	    (new-host-info (make-hash-table :test #'equal)))
	(cl-loop for source across sources do
		 ;; FIXME: is there a merge function that can be used
		 ;; here instead of this awkward unpack?
		 (puthash "schema"
			  (gethash "schema"
				   (redashctl-get-schema redash-url
							 (gethash "id" source)))
			  source))
	(puthash "sources" sources new-host-info)
	(puthash "last_fetched" (redashctl--as-iso-8601) new-host-info)
	(puthash redash-url new-host-info redashctl--info-cache)
	(setf host-info new-host-info)))
    host-info))

(defun redashctl-get-sources-for-host (redash-url)
  (cl-map 'list
	  (lambda (x) (gethash "name" x))
	  (gethash "sources" (redashctl--get-host-info redash-url))))

(defun redashctl-get-source-by-id (redash-url source-id)
  (cl-loop for source across (gethash "sources" (redashctl--get-host-info redash-url))
	   when (= source-id (gethash "id" source))
	   return source))

(defun redashctl-get-source-id-by-name (redash-url source-name)
  (cl-loop for source across (gethash "sources" (redashctl--get-host-info redash-url))
	   when (string= source-name (gethash "name" source))
	   return (gethash "id" source)))

(defun redashctl-set-current-query-source (new-source)
  (interactive (list (completing-read "Query source to use: "
				      (redashctl-get-sources-for-url redashctl-current-url)
				      nil
				      t)))
  (setf redashctl-current-query-source
	(redashctl-get-source-id-by-name redashctl-current-url new-source)))

(defun redashctl-get-hosts ()
  (cl-remove-duplicates (append redashctl-urls
				(cl-loop for host being the hash-keys in redashctl--info-cache
					 collect host))
			:test #'string=))

(defun redashctl-set-current-url (new-url)
  (interactive (list (completing-read "URL to use: "
				      (redashctl-get-hosts))))
  (setf redashctl-current-url new-url)
  (when (called-interactively-p)
    (call-interactively #'redashctl-set-current-query-source)))

(defun redashctl-write-query-results (redash-url query-result-id)
  (let* ((raw-query-result (gethash "query_result"
				    (redashctl-get-query-results redash-url query-result-id)))
	 (query-data (gethash "data" raw-query-result))
	 (query-schema (gethash "columns" query-data))
	 (query-columns (cl-map 'vector
				(lambda (x)
				  (gethash "friendly_name" x))
				query-schema))
	 (source-info (redashctl-get-source-by-id redash-url (gethash "data_source_id" raw-query-result))))
    (with-current-buffer (get-buffer-create (redashctl--results-buffer-for-url redash-url))
      (org-mode)
      (erase-buffer)
      (insert "# -*- mode: org -*-\n")
      (insert "* Metadata\n")
      (insert "#+PROPERTY: VISIBILITY folded\n")
      (insert ":PROPERTIES:\n")
      (cl-loop for k being the hash-keys in raw-query-result using (hash-value v)
	       unless (member k '("query" "data"))
	       do (insert (format ":%s: %s\n" k v)))
      (insert ":END:\n")
      (insert "* Query\n")
      (insert "#+BEGIN_SRC " (gethash "syntax" source-info) "\n")
      (insert (gethash "query" raw-query-result) "\n")
      (insert "#+END_SRC\n")
      (insert "* Schema\n")
      (insert "#+PROPERTY: VISIBILITY folded\n")
      (cl-loop for col across query-schema
	       for idx = 0 then (1+ idx)
	       do
	       (insert (format "** Column %d\n" idx) ":PROPERTIES:\n")
	       (cl-loop for k being the hash-keys in col using (hash-value v) do
			(insert (format ":%s: %s\n" k v)))
	       (insert ":END:\n"))
      (insert "* Results\n")
      (cl-loop for row across (gethash "rows" query-data)
	       for idx = 0 then (1+ idx)
	       do
	       (insert (format "** Row %d\n" idx) ":PROPERTIES:\n")
	       (cl-loop for col across query-columns do
			(insert (format ":%s: %s\n"
					col
					(gethash col row))))
	       (insert ":END:\n"))
      ;; (let ((org-startup-folded nil))
      ;; 	(org-cycle-set-startup-visibility))
      )))

(defun redashctl-show-results-for-url (query-url)
  (interactive "MQuery URL: ")
  ;"http://localhost:5000/queries/1"
  (save-match-data
    (unless (string-match "\\(.*\\)/queries/\\([[:digit:]]+\\)" query-url)
      (user-error "Not a Redash URL"))
    (let* ((base-url (match-string 1 query-url))
	   (query-id (match-string 2 query-url))
	   (query-data (redashctl-get-query base-url query-id)))
      (redashctl-show-results-for-query-id base-url
					   (gethash "latest_query_data_id"
						    query-data)))))

(defun redashctl-show-results-for-results-id (redash-url results-id)
  (redashctl-write-query-results redash-url results-id)
  (pop-to-buffer (redashctl--results-buffer-for-url redash-url)))

(defsubst redashctl--setup-auto-queue-scan-timer ()
  (run-at-time 0 1 #'redashctl-show-results-for-job-queue))

(defun redashctl-add-job-to-queue (job-id)
  (setf redashctl--job-queue
	(append redashctl--job-queue
		(list job-id)))
  ;; enable auto job scan, if not yet enabled and not explicitly disabled
  (when (and (not redashctl--job-scan-timer)
	     redashctl-enable-auto-job-scan)
    (redashctl--setup-auto-queue-scan-timer)))

(defun redashctl-show-results-for-job-queue ()
  (interactive)
  (when redashctl--job-queue
    (let* ((job (redashctl-get-job redashctl-current-url (cl-first redashctl--job-queue)))
	   (results-id (gethash "query_result_id" (gethash "job" job))))
      (unless (eq results-id :null)
	(redashctl-show-results-for-results-id redashctl-current-url results-id)
	(pop redashctl--job-queue)))))

(defun redashctl-toggle-auto-queue-scan ()
  (interactive)
  (setf redashctl--job-scan-timer
	(if (member redashctl--job-scan-timer '(:disabled nil))
	    (redashctl--setup-auto-queue-scan-timer)
	  (progn (cancel-timer redashctl--job-scan-timer)
		 :disabled))))

(defun redashctl-send-query-region (start end)
  (interactive "r")
  (let ((job-result (redashctl-run-query redashctl-current-url
					 start
					 end
					 redashctl-current-query-source)))
    (redashctl-add-job-to-queue (gethash "id" (gethash "job" job-result)))))

(defun redashctl-send-query-buffer ()
  (interactive)
  (redashctl-send-query-region (point-min) (point-max)))

(defun redashctl--show-cache-as-json ()
  "debugging function"
  (with-current-buffer (get-buffer-create (format "*redash-cache*"))
    (erase-buffer)
    (insert (json-serialize redashctl--info-cache))
    (shell-command-on-region (point-min) (point-max) "python3 -m json.tool" (current-buffer) t)))

(provide 'redashctl)

